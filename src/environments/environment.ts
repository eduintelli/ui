// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --configuration=production` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebaseConfig: {
    apiKey: "AIzaSyCxKUF5eXs8VW4OckXDXX2EZaV9XO4f7e8",
    authDomain: "dev-eduintelli.firebaseapp.com",
    databaseURL: "https://dev-eduintelli.firebaseio.com",
    projectId: "dev-eduintelli",
    storageBucket: "dev-eduintelli.appspot.com",
    messagingSenderId: "52090360458",
    appId: "1:52090360458:web:2a94bd9d9da02bd6"
  }
};
