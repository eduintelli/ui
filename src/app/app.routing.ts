import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { SignInComponent } from './signing/sign-in/sign-in.component';
import { SignUpComponent } from './signing/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './signing/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './signing/verify-email/verify-email.component';
// Import canActivate guard services
import { AuthGuard } from './shared/guard/auth.guard';
import { SecureInnerPagesGuard } from './shared/guard/secure-inner-pages.guard';
import { ProfileComponent } from './profile/profile.component';
import { PrivacyComponent } from './shared/privacy/privacy.component';
import { TermsComponent } from './shared/terms/terms.component';
import {SecPredictComponent} from './sec-predict/sec-predict.component';
import {SecPostingComponent} from './sec-posting/sec-posting.component';
import { NavigatorComponent } from './navigator/navigator.component';

const routes: Routes = [
  { path: 'psle-predict', component: HomeComponent },
  { path: '', redirectTo: 'navigator', pathMatch: 'full' },
  { path: 'profile', component: ProfileComponent, canActivate: [AuthGuard] },
  { path: 'sign-in', component: SignInComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'register-user', component: SignUpComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'forgot-password', component: ForgotPasswordComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'verify-email-address', component: VerifyEmailComponent, canActivate: [SecureInnerPagesGuard] },
  { path: 'privacy-policy', component: PrivacyComponent },
  { path: 'terms-of-service', component: TermsComponent },
  { path: 'sec-predict', component: SecPredictComponent, canActivate: [AuthGuard] },
  { path: 'sec-posting', component: SecPostingComponent, canActivate: [AuthGuard] },
  { path: 'navigator', component: NavigatorComponent },
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes, { useHash: true })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
