import { Component, OnInit } from '@angular/core';
import { AuthService } from '../services/auth.service';
import { UserService } from '../services/user.service';
import { User } from '../services/user';
import { CalculatedScore } from 'app/data/calc-score';
import { MatTableDataSource } from '@angular/material';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { FormControl } from '@angular/forms';
import { CoreService } from 'app/services/core.service';
import { MatProgressButtonOptions } from 'mat-progress-buttons'
import { GoogleanalyticsService } from 'app/services/googleanalytics.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})

export class ProfileComponent implements OnInit {

  user: User;
  displayedColumns: string[] = ['grade', 'math', 'science', 'eng', 'mt', 'actions'];
  dataSource: Array<CalculatedScore>;
  tableSource: any;
  private primaryList = [];
  filteredOptions: Observable<string[]>;
  primarySchoolName: string;
  primarySchoolCtrl = new FormControl();
  gradetermCtrl = new FormControl();
  mathCtrl = new FormControl();
  scienceCtrl = new FormControl();
  engCtrl = new FormControl();
  mtCtrl = new FormControl();
  public gradeterms = [
    { display: 'P5-SA1', grade: '5', term: 'SA1' },
    { display: 'P5-SA2', grade: '5', term: 'SA2' },
    { display: 'P6-SA1', grade: '6', term: 'SA1' },
    { display: 'P6-Prelim', grade: '6', term: 'SA2' }
  ];

  saveButtonOptions: MatProgressButtonOptions = {
    active: false,
    text: 'Save Changes',
    buttonColor: 'primary',
    barColor: 'accent',
    raised: true,
    stroked: false,
    mode: 'indeterminate',
    value: 0,
    disabled: false
  }

  panelOpenState: any;

  constructor(
    public userService: UserService,
    public authService: AuthService,
    private coreService: CoreService,
    public googleAnalyticsService: GoogleanalyticsService
  ) {
    this.user = authService.userData;
  }

  ngOnInit() {
    this.filteredOptions = this.primarySchoolCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value))
    );
    this.coreService.getSchoolList()
      .subscribe(res => {
        this.primaryList = res.schools;
      }, err => {
        console.log(err);
      });

    this.RenderUserData();
  }

  deleteRow(id) {
    this.dataSource.splice(id, 1);
    this.tableSource = new MatTableDataSource<CalculatedScore>(this.dataSource);
  }

  RenderUserData() {
    this.userService.GetUserData(this.user)
      .subscribe(
        res => {
          if (res.exists) {
            this.user = JSON.parse(JSON.stringify(res.data()));
            this.dataSource = this.user.scores;
            this.tableSource = new MatTableDataSource<CalculatedScore>(this.dataSource);
            if (this.user.school) {
              this.primarySchoolCtrl.setValue(this.user.school);
            }
          }
        },
        error => {
          console.log('There was an error while retrieving data !!!' + error);
        });
  }
  private _filter(value: string): string[] {
    if (value) {
      const filterValue = value.toLowerCase();
      return this.primaryList.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
    } else {

    }
  }
  addScore() {
    let newScore = new CalculatedScore();
    newScore.grade = this.gradetermCtrl.value['grade'];
    newScore.term = this.gradetermCtrl.value['term'];
    newScore.eng = this.engCtrl.value;
    newScore.math = this.mathCtrl.value;
    newScore.mt = this.mtCtrl.value;
    newScore.science = this.scienceCtrl.value;

    let matched = false;
    for (let ele of this.dataSource) {
      if (ele.grade === newScore.grade && ele.term === newScore.term) {
        matched = true;
      }
    }
    if (!matched) {
      this.dataSource.push(newScore);
      this.tableSource = new MatTableDataSource<CalculatedScore>(this.dataSource);
      this.gradetermCtrl.setValue(0);
      this.engCtrl.setValue(0);
      this.mathCtrl.setValue(0);
      this.mtCtrl.setValue(0);
      this.scienceCtrl.setValue(0);
    }
    else {

    }
  }
  saveChanges() {
    this.googleAnalyticsService.eventEmitter('profilepage', 'savechanges', 'btn', 1);
    this.saveButtonOptions.active = true;
    this.user.school = this.primarySchoolCtrl.value;
    this.user.scores = JSON.parse(JSON.stringify(this.dataSource));
    this.userService.SaveUserData(this.user)
    setTimeout(() => {
      this.saveButtonOptions.active = false;
    },
      1500);
  }

}
