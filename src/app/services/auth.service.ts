import { Injectable, NgZone } from '@angular/core';
import { User } from '../services/user';
import { auth } from 'firebase/app';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore, AngularFirestoreDocument } from '@angular/fire/firestore';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  user: any; // Save logged in user data
  constructor(
    public afs: AngularFirestore,   // Inject Firestore service
    public afAuth: AngularFireAuth, // Inject Firebase auth service
    public router: Router,
    public ngZone: NgZone // NgZone service to remove outside scope warning
  ) {
    /* Saving user data in localstorage when 
    logged in and setting up null when logged out */
    this.afAuth.authState.subscribe(user => {
      if (user) {
        this.user = user;
        localStorage.setItem('user', JSON.stringify(this.user));
        JSON.parse(localStorage.getItem('user'));
      } else {
        localStorage.setItem('user', null);
        JSON.parse(localStorage.getItem('user'));
      }
    })
  }
  // Sign in with email/password
  SignIn(email, password) {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password)
      .then((result) => {
        this.SetUserData(result.user).then(x => {
          this.ngZone.run(() => {
            setTimeout(() => {
              this.router.navigate(['sec-predict']);
            },
            2000);
          })
        });
      }).catch((error) => {
        window.alert(error.message)
      })
  }
  // Sign up with email/password
  SignUp(email, password) {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password)
      .then((result) => {
        /* Call the SendVerificaitonMail() function when new user sign 
        up and returns promise */
        this.SendVerificationMail();
        this.SetUserData(result.user);
      }).catch((error) => {
        window.alert(error.message)
      })
  }
  // Send email verfificaiton when new user sign up
  SendVerificationMail() {
    return this.afAuth.auth.currentUser.sendEmailVerification()
      .then(() => {
        this.router.navigate(['verify-email-address']);
      })
  }
  // Reset Forggot password
  ForgotPassword(passwordResetEmail) {
    return this.afAuth.auth.sendPasswordResetEmail(passwordResetEmail)
      .then(() => {
        window.alert('Password reset email sent, check your inbox.');
      }).catch((error) => {
        window.alert(error)
      })
  }
  // Returns true when user is looged in and email is verified
  get isLoggedIn(): boolean {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null) ? true : false;
  }
  // Returns true when user is looged in and email is verified
  get userInfo(): User {
    const user = JSON.parse(localStorage.getItem('user'));
    return (user !== null) ? user : null;
  }
  get userData(): User {
    const user = JSON.parse(localStorage.getItem('userData'));
    return (user !== null) ? user : null;
  }
  // Sign in with Google
  GoogleAuth() {
    return this.AuthLogin(new auth.GoogleAuthProvider());
  }
  // Sign in with Google
  FacebookAuth() {
    return this.AuthLogin(new auth.FacebookAuthProvider());
  }
  // Auth logic to run auth providers
  AuthLogin(provider) {
    return this.afAuth.auth.signInWithPopup(provider)
      .then((result) => {
        this.SetUserData(result.user).then(x => {
          this.ngZone.run(() => {
            setTimeout(() => {
              this.router.navigate(['sec-predict']);
            },
            2000);
          })
        });
      }).catch((error) => {
        window.alert(error)
      })
  }
  /* Setting up user data when sign in with username/password, 
  sign up with username/password and sign in with social auth  
  provider in Firestore database using AngularFirestore + AngularFirestoreDocument service */
  async SetUserData(user) {
    const userRef: AngularFirestoreDocument<any> = this.afs.doc(`users/${user.uid}`);
    await userRef.get().subscribe(function (doc) {
      if (doc.exists) {
        localStorage.setItem('userData', JSON.stringify(doc.data()));
      } else {
        // doc.data() will be undefined in this case
        const userData: User = {
          uid: user.uid,
          email: user.email,
          displayName: user.displayName,
          photoURL: user.photoURL,
          emailVerified: user.emailVerified,
          phoneNumber: user.phoneNumber,
          postalCode: null,
          scores: [],
          school: null,
          timeStamp: new Date().toLocaleString()
        }
        const lastCalculatedScore = JSON.parse(localStorage.getItem('calculatedscore'));
        if (lastCalculatedScore != null) {
          userData.scores.push(lastCalculatedScore);
        }
        const school = localStorage.getItem('school');
        if (school != null) {
          userData.school = school;
        }
        const postalCode = localStorage.getItem('postalcode');
        if (postalCode != null) {
          userData.postalCode = postalCode;
        }
        localStorage.setItem('userData', JSON.stringify(userData));
        return userRef.set(userData, {
          merge: false
        })
      }

    });
    /*
      const userData: User = {
        uid: user.uid,
        email: user.email,
        displayName: user.displayName,
        photoURL: user.photoURL,
        emailVerified: user.emailVerified,
        phoneNumber: user.phoneNumber,
        postalCode: null,
        scores: []
      }
      const lastCalculatedScore = JSON.parse(localStorage.getItem('calculatedscore'));
      if (lastCalculatedScore != null) {
        userData.scores.push(lastCalculatedScore);
      }
      
      return userRef.set(userData, {
        merge: false
      })
      */

  }
  // Sign out
  SignOut() {
    return this.afAuth.auth.signOut().then(() => {
      localStorage.removeItem('user');
      localStorage.removeItem('userData');
      this.router.navigate(['navigator']);
    })
  }
}
