import { CalculatedScore } from 'app/data/calc-score';

export interface User {
    uid: string;
    email: string;
    displayName: string;
    photoURL: string;
    emailVerified: boolean;
    phoneNumber: string;
    postalCode: string;
    scores: Array<CalculatedScore>;
    school: string;
    timeStamp: string;
}
