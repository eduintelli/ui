import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Injectable, Inject } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';
import { Schoollist } from '../data/schoollist';
import { ScoreSets } from '../data/score-sets';
import { ResScore } from '../data/res-score';
import { stringify } from '@angular/core/src/render3/util';
import { CalcSecResult } from 'app/data/calc-sec-result';
import { CalcSecReq } from 'app/data/calc-sec-req';
import { CalcSeclistReq } from 'app/data/calc-seclist-req';
import { CalcSeclistRes } from 'app/data/calc-seclist-res';
import { PostSeclistReq } from 'app/data/post-seclist-req';
import { PostSeclistRes } from 'app/data/post-seclist-res';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
const srvUrl = 'https://sgdatascience.appspot.com';

@Injectable({
  providedIn: 'root'
})
export class CoreService {

  private clientSeedKey = 'sId';
  constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private httpClient: HttpClient) { }

  getSchoolList(): Observable<Schoollist> {
    const apiUrl = srvUrl + '/Get_PSch';
    return this.httpClient.post(apiUrl, { 'key': 'gYdK$q12@as' }, httpOptions).pipe(
      tap((data: Schoollist) => console.log('')),
      catchError(this.handleError<Schoollist>('getSchool'))
    );
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  getClientSeed(): any {
    return this.localStorage.getItem(this.clientSeedKey);

  }

  setClientSeed(seed: string): void {
    return this.localStorage.setItem(this.clientSeedKey, seed);

  }

  getClientIp() {
    return this.httpClient.get('https://api.ipify.org?format=json');
  }

  generateUnitId(len: number) {
    let text = '';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    for (let i = 0; i < len; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  getTscore(record: ScoreSets): Observable<ResScore> {
    const apiUrl = srvUrl + '/create_scores';
    return this.httpClient.post(apiUrl, record, httpOptions).pipe(
      tap((data: ResScore) => console.log('')),
      catchError(this.handleError<ResScore>('getTscore'))
    );
  }

  getRestrictedTscore(record: ScoreSets): Observable<ResScore> {
    const apiUrl = srvUrl + '/create_scores_w_counter';
    return this.httpClient.post(apiUrl, record, httpOptions).pipe(
      tap((data: ResScore) => console.log('')),
      catchError(this.handleError<ResScore>('getTscore'))
    );
  }

  calcSecChance(record: CalcSecReq): Observable<CalcSecResult[]> {
    const apiUrl = srvUrl + '/cal_sec';
    return this.httpClient.post(apiUrl, record, httpOptions).pipe(
      tap((data: CalcSecResult[]) => console.log('')),
      catchError(this.handleError<CalcSecResult[]>('calcSecChance'))
    );
  }

  getSecondaySchoolList(): Observable<Schoollist> {
    const apiUrl = srvUrl + '/Get_SSch';
    return this.httpClient.post(apiUrl, { 'key': 'gYdK$q12@as' }, httpOptions).pipe(
      tap((data: Schoollist) => console.log('')),
      catchError(this.handleError<Schoollist>('getSecondarySchool'))
    );
  }
  getSecondaryPrediction(record: CalcSeclistReq): Observable<Array<CalcSeclistRes>> {
    const apiUrl = srvUrl + '/cal_sec_list';
    return this.httpClient.post(apiUrl, record, httpOptions).pipe(
      tap((data: Array<CalcSeclistRes>) => console.log('')),
      catchError(this.handleError<Array<CalcSeclistRes>>('getSecondaryPrediction'))
    );
  }
  getSecondaryPosting(record: PostSeclistReq): Observable<Array<PostSeclistRes>> {
    const apiUrl = srvUrl + '/cal_sec_post';
    return this.httpClient.post(apiUrl, record, httpOptions).pipe(
      tap((data: Array<PostSeclistRes>) => console.log('')),
      catchError(this.handleError<Array<PostSeclistRes>>('getSecondaryPosting'))
    );
  }
}
