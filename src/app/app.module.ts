import { NgtUniversalModule } from '@ng-toolkit/universal';
import { CommonModule } from '@angular/common';
import { TransferHttpCacheModule } from '@nguniversal/common';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { AppRoutingModule } from './app.routing';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { FooterComponent, AboutUsContent, DisclaimerContent } from './shared/footer/footer.component';
import { HomeModule } from './home/home.module';

import { NgxPageScrollCoreModule } from 'ngx-page-scroll-core';
import { NgxPageScrollModule } from 'ngx-page-scroll';

import 'hammerjs';

// angular material modules
import {

  MatAutocompleteModule,

  MatButtonModule,

  MatButtonToggleModule,

  MatCardModule,

  MatCheckboxModule,

  MatChipsModule,

  MatDatepickerModule,

  MatDialogModule,

  MatDividerModule,

  MatExpansionModule,

  MatGridListModule,

  MatIconModule,

  MatInputModule,

  MatListModule,

  MatMenuModule,

  MatNativeDateModule,

  MatPaginatorModule,

  MatProgressBarModule,

  MatProgressSpinnerModule,

  MatRadioModule,

  MatRippleModule,

  MatSelectModule,

  MatSidenavModule,

  MatSliderModule,

  MatSlideToggleModule,

  MatSnackBarModule,

  MatSortModule,

  MatStepperModule,

  MatTableModule,

  MatTabsModule,

  MatToolbarModule,

  MatTooltipModule,
  MatFormFieldModule,

} from '@angular/material';

// Firebase services + enviorment module
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { environment } from '../environments/environment';
import { SignInComponent } from './signing/sign-in/sign-in.component';
import { SignUpComponent } from './signing/sign-up/sign-up.component';
import { ForgotPasswordComponent } from './signing/forgot-password/forgot-password.component';
import { VerifyEmailComponent } from './signing/verify-email/verify-email.component';
import { ProfileComponent } from './profile/profile.component';
import { AuthService } from './services/auth.service';
import { PrivacyComponent } from './shared/privacy/privacy.component';
import { TermsComponent } from './shared/terms/terms.component';
import { MatProgressButtonsModule } from 'mat-progress-buttons';
import { SecPredictComponent } from './sec-predict/sec-predict.component';
import { AgmCoreModule } from '@agm/core';
import { GoogleanalyticsService } from './services/googleanalytics.service';
import { SecPostingComponent } from './sec-posting/sec-posting.component';
import { NavigatorComponent } from './navigator/navigator.component';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FooterComponent,
    AboutUsContent,
    DisclaimerContent,
    SignInComponent,
    SignUpComponent,
    ForgotPasswordComponent,
    VerifyEmailComponent,
    ProfileComponent,
    PrivacyComponent,
    TermsComponent,
    SecPredictComponent,
    SecPostingComponent,
    NavigatorComponent,
  ],
  imports: [
    CommonModule,
    NgtUniversalModule,
    TransferHttpCacheModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatIconModule,
    MatMenuModule,
    MatInputModule,
    MatCardModule,
    MatDividerModule,
    MatFormFieldModule,
    MatTableModule,
    MatExpansionModule,
    MatAutocompleteModule,
    MatSelectModule,
    HttpClientModule,
    NgbModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    AppRoutingModule,
    HomeModule,
    NgxPageScrollCoreModule.forRoot(),
    NgxPageScrollModule,
    MatButtonModule,
    MatDialogModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,
    AngularFirestoreModule,
    MatProgressButtonsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyC-W8CpKyvVk4Pwsq1m4KqNfAHCYZ_69aw',
      libraries: ['places']
    }),
  ],
  entryComponents: [AboutUsContent, DisclaimerContent],
  providers: [
    AuthService,
    GoogleanalyticsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
