import { Component, OnInit, Inject, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserService } from 'app/services/user.service';
import { CoreService } from 'app/services/core.service';
import { map, startWith } from 'rxjs/operators';
import { MatProgressButtonOptions } from 'mat-progress-buttons'
import { MapsAPILoader } from '@agm/core';
import { User } from 'app/services/user';
import { AuthService } from 'app/services/auth.service';
import { GoogleanalyticsService } from 'app/services/googleanalytics.service';
import { PostSeclistRes } from 'app/data/post-seclist-res';
import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { PostSeclistReq } from 'app/data/post-seclist-req';

@Component({
  selector: 'app-sec-posting',
  templateUrl: './sec-posting.component.html',
  styleUrls: ['./sec-posting.component.scss']
})
export class SecPostingComponent implements OnInit {

  user: User;
  public lat: number;
  public lng: number;
  public zoom: number;
  public markers: Array<PostSeclistRes> = [];
  isValidCode = false;
  public score: number;
  public selectedGender: string;

  postalcodeButtonOptions: MatProgressButtonOptions = {
    active: false,
    text: 'Search',
    buttonColor: 'primary',
    barColor: 'accent',
    raised: true,
    stroked: false,
    mode: 'indeterminate',
    value: 0,
    disabled: false
}

  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(
    @Inject(LOCAL_STORAGE) private localStorage: any,
    private userService: UserService,
    private coreService: CoreService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private authService: AuthService,
    public googleAnalyticsService: GoogleanalyticsService
  ) {
    this.user = this.authService.userData;
  }

  ngOnInit() {
    this.lat = 1.3521;
    this.lng = 103.8198;
    this.zoom = 11;
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      //this.setCurrentLocation();
      //this.geoCoder = new google.maps.Geocoder;
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
          types: ["geocode"]
      });
      autocomplete.setComponentRestrictions(
          { 'country': ['sg'] });
      this.isValidCode = false;
      autocomplete.addListener("place_changed", () => {
          this.ngZone.run(() => {
              //get the place result
              let place: google.maps.places.PlaceResult = autocomplete.getPlace();
              //verify result
              if (place.geometry === undefined || place.geometry === null) {
                  this.isValidCode = false;
                  return;
              }
              //set latitude, longitude and zoom
              this.lat = place.geometry.location.lat();
              this.lng = place.geometry.location.lng();
              this.zoom = 11;
              this.isValidCode = true;
              localStorage.setItem('postalcode', place.name);
          });
      });
  });
  }

  postalCodeSet(): void {
    this.googleAnalyticsService.eventEmitter('secpostingadvisor', 'post-querynearby', 'btn', 1);
    this.postalcodeButtonOptions.active = true;
    let requiredData = new PostSeclistReq();
    requiredData.score = this.score;
    requiredData.gender = this.selectedGender;
    requiredData.lat = this.lat;
    requiredData.lng = this.lng;
    requiredData.key = 'gYdK$q12@as';
    this.coreService.getSecondaryPosting(requiredData)
        .subscribe(res => {
            this.markers = res;
            // format chance number
            let index = 0;
            for (let marker of this.markers) {
              index = index + 1;
              marker.label = index.toString();
              marker.chance = Number((marker.chance * 100).toFixed(1))
            }
            this.postalcodeButtonOptions.active = false;
        }, err => {
            console.log(err);
            this.postalcodeButtonOptions.active = false;
        });
  }

  openedWindow: number = 0; // alternative: array of numbers

  openWindow(id) {
      this.openedWindow = id; // alternative: push to array of numbers
  }

  isInfoWindowOpen(id) {
      return this.openedWindow == id; // alternative: check if id is in array
  }

}
