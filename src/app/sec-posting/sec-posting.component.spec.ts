import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecPostingComponent } from './sec-posting.component';

describe('SecPostingComponent', () => {
  let component: SecPostingComponent;
  let fixture: ComponentFixture<SecPostingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecPostingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecPostingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
