import { Component, OnInit, Inject, Input } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'about-us-content',
    template: `
    <div class="modal-header">
      <h4 class="modal-title">About Us</h4>
    </div>
    <div class="modal-body">
      <p>PSLE score prediction is a data science model developed by www.sgdatascience.com group.
      The model takes a student's past exam score as input and compares with cohort's performance.
      Please type in your most recent Semestral Assessment (SA) score of four subjects (English, Maths,
      Science and Mother Tongue) and the name of your primary school, the model will predict your future PSLE
      scores. Have a try!
      </p>
    </div>
  `
  })
  export class AboutUsContent {
    @Input() name;
    constructor(public activeModal: NgbActiveModal) {}
  }

  @Component({
    selector: 'Disclaimer-content',
    template: `
    <div class="modal-header">
      <h4 class="modal-title">Disclaimer</h4>
    </div>
    <div class="modal-body">
      <p>PSLE score prediction is a statistic data science development project.
      We do not make any warranties about the completeness, reliability and accuracy of the information
      provided on this website. Any action you take upon is at your own risk, and we will not be liable for any loss
      and damages in connection with the use of our websites.
      </p>
    </div>
  `
  })
  export class DisclaimerContent {
    @Input() name;
    constructor(public activeModal: NgbActiveModal) {}
  }

@Component({
    selector: 'app-footer',
    templateUrl: './footer.component.html',
    styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {
    test: Date = new Date();

    constructor(private modalService: NgbModal) { }

    ngOnInit() {}

    openAboutUs() {
        const modalRef = this.modalService.open(AboutUsContent);
    }

    openDisclaimer() {
        const modalRef = this.modalService.open(DisclaimerContent);
    }
    mailWithLocation() {
      location.href = "mailto:feedbacksgds@gmail.com?subject=Feedback&body=Hi"
   }
}


