import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './home.component';

import { ComponentsModule } from '../components/components.module';

import { CdkStepperModule } from '@angular/cdk/stepper';

import {

    MatAutocompleteModule,

    MatButtonModule,

    MatButtonToggleModule,

    MatCardModule,

    MatCheckboxModule,

    MatChipsModule,

    MatDatepickerModule,

    MatDialogModule,

    MatDividerModule,

    MatExpansionModule,

    MatGridListModule,

    MatIconModule,

    MatInputModule,

    MatListModule,

    MatMenuModule,

    MatNativeDateModule,

    MatPaginatorModule,

    MatProgressBarModule,

    MatProgressSpinnerModule,

    MatRadioModule,

    MatRippleModule,

    MatSelectModule,

    MatSidenavModule,

    MatSliderModule,

    MatSlideToggleModule,

    MatSnackBarModule,

    MatSortModule,

    MatStepperModule,

    MatTableModule,

    MatTabsModule,

    MatToolbarModule,

    MatTooltipModule,

} from '@angular/material';

import { CdkTableModule } from '@angular/cdk/table';

import 'hammerjs';

import { AgmCoreModule } from '@agm/core';

import { MatProgressButtonsModule } from 'mat-progress-buttons';
import { GoogleanalyticsService } from 'app/services/googleanalytics.service';

@NgModule({
    imports: [
        NgbModule,
        CommonModule,
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        RouterModule,
        ComponentsModule,
        ReactiveFormsModule,

        CdkStepperModule,

        CdkTableModule,

        MatAutocompleteModule,

        MatButtonModule,

        MatButtonToggleModule,

        MatCardModule,

        MatCheckboxModule,

        MatChipsModule,

        MatStepperModule,

        MatDatepickerModule,

        MatDialogModule,

        MatDividerModule,

        MatExpansionModule,

        MatGridListModule,

        MatIconModule,

        MatInputModule,

        MatListModule,

        MatMenuModule,

        MatNativeDateModule,

        MatPaginatorModule,

        MatProgressBarModule,

        MatProgressSpinnerModule,

        MatRadioModule,

        MatRippleModule,

        MatSelectModule,

        MatSidenavModule,

        MatSliderModule,

        MatSlideToggleModule,

        MatSnackBarModule,

        MatSortModule,

        MatTableModule,

        MatTabsModule,

        MatToolbarModule,

        MatTooltipModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyC-W8CpKyvVk4Pwsq1m4KqNfAHCYZ_69aw',
            libraries: ['places']
          }),
        MatProgressButtonsModule,
    ],
    declarations: [HomeComponent],
    exports: [HomeComponent],
    providers: [GoogleanalyticsService]
})
export class HomeModule { }
