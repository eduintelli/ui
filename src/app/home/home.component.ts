import { LOCAL_STORAGE } from '@ng-toolkit/universal';
import { Component, OnInit, Inject, ViewChild, ElementRef, NgZone } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Observable } from 'rxjs';
import { map, startWith } from 'rxjs/operators';
import { CoreService } from '../services/core.service'
import { Schoollist } from '../data/schoollist';

import { PageScrollService } from 'ngx-page-scroll-core';

import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { ScoreSets } from 'app/data/score-sets';
import { ScoreSet } from 'app/data/score-set';
import { Md5 } from 'ts-md5/dist/md5';
import { MapsAPILoader } from '@agm/core';
import { MatProgressButtonOptions } from 'mat-progress-buttons'
import { CalcSecReq } from 'app/data/calc-sec-req';
import { CalcSecResult } from 'app/data/calc-sec-result';
import { CalculatedScore } from 'app/data/calc-score';
import { AuthService } from '../services/auth.service';
import { GoogleanalyticsService } from '../services/googleanalytics.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { environment } from '../../environments/environment';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})

export class HomeComponent implements OnInit {

    private list = [];
    data: Schoollist;
    model: any;

    isLinear = true;
    public scoreFormGroup: FormGroup;
    public schoolFormGroup: FormGroup;
    filteredOptions: Observable<string[]>;

    public spinnerColor = 'primary';
    public spinnerMode = 'indeterminate';
    public spinnerValue = 25;
    public spinnerLoading = false;
    public isCollapsed = true;
    public gradeterms = [
        { display: 'P5-SA1', grade: '5', term: 'SA1' },
        { display: 'P5-SA2', grade: '5', term: 'SA2' },
        { display: 'P6-SA1', grade: '6', term: 'SA1' },
        { display: 'P6-Prelim', grade: '6', term: 'SA2' }];
    public score: number;
    public percent: string;
    public stdev: number;
    private selectedGradeTerm: {};

    isShowMap = false;
    isValidCode = false;
    public lat: number;
    public lng: number;
    public zoom: number;
    public markers: Array<CalcSecResult> = [];

    postalcodeButtonOptions: MatProgressButtonOptions = {
        active: false,
        text: 'Search',
        buttonColor: 'primary',
        barColor: 'accent',
        raised: true,
        stroked: false,
        mode: 'indeterminate',
        value: 0,
        disabled: false
    }

    calculatedScore: CalculatedScore;

    @ViewChild('search')
    public searchElementRef: ElementRef;

    constructor(@Inject(LOCAL_STORAGE) private localStorage: any, private coreService: CoreService,
        private pageScrollService: PageScrollService, @Inject(DOCUMENT) private document: any,
        private _formBuilder: FormBuilder,
        private mapsAPILoader: MapsAPILoader,
        private ngZone: NgZone,
        public authService: AuthService,
        public googleAnalyticsService: GoogleanalyticsService,
        private _snackBar: MatSnackBar
    ) {
        if (!environment.production) {
            const alertString = `Our offical site is ready, please visit www.sgdatascience.com;
            This site is sgdatascience's testing site for new functions before release.`
            let snackBarRef = this._snackBar.open(alertString, 'GoTo', {
                duration: 20000,
            });
            snackBarRef.onAction().subscribe(() => {
                this.document.location.href = 'https://www.sgdatascience.com';
            });
        }
     }

    ngOnInit() {
        this.calculatedScore = new CalculatedScore();

        this.score = 0;
        this.coreService.getSchoolList()
            .subscribe(res => {
                this.data = res;
                this.list = this.data.schools;
            }, err => {
                console.log(err);
            });

        this.scoreFormGroup = this._formBuilder.group({
            scores: this._formBuilder.array([
                this.initScore()
            ])
        });

        this.schoolFormGroup = this._formBuilder.group({
            schoolName: ['', Validators.required]
        });

        this.filteredOptions = this.schoolFormGroup.controls.schoolName.valueChanges.pipe(
            startWith(''),
            map(value => this._filter(value))
        );

        const seed = this.coreService.getClientSeed();
        if (!seed) {
            this.coreService.getClientIp()
                .subscribe(res => {
                    const ip = res['ip'];
                    const md5 = new Md5();
                    this.coreService.setClientSeed(md5.appendStr(ip).end().toString());
                }, err => {
                    console.log(err);
                });
        }
        //default grade selection
        this.scoreFormGroup['controls'].scores['controls'][0]['controls'].gradeterm.value = this.gradeterms[3];
        //default map location Singapore
        this.lat = 1.3521;
        this.lng = 103.8198;
        this.zoom = 11;

        //load Places Autocomplete
        this.mapsAPILoader.load().then(() => {
            //this.setCurrentLocation();
            //this.geoCoder = new google.maps.Geocoder;
            let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
                types: ["geocode"]
            });
            autocomplete.setComponentRestrictions(
                { 'country': ['sg'] });
            this.isValidCode = false;
            autocomplete.addListener("place_changed", () => {
                this.ngZone.run(() => {
                    //get the place result
                    let place: google.maps.places.PlaceResult = autocomplete.getPlace();
                    //verify result
                    if (place.geometry === undefined || place.geometry === null) {
                        this.isValidCode = false;
                        return;
                    }
                    //set latitude, longitude and zoom
                    this.lat = place.geometry.location.lat();
                    this.lng = place.geometry.location.lng();
                    this.zoom = 14;
                    this.isValidCode = true;
                    localStorage.setItem('postalcode', place.name);
                });
            });
        });
    }

    postalCodeSet(): void {
        this.googleAnalyticsService.eventEmitter('homepage', 'querynearby', 'btn', 1);
        this.postalcodeButtonOptions.active = true;
        let calcRequiredData = new CalcSecReq();
        calcRequiredData.score = this.score;
        calcRequiredData.stdev = this.stdev;
        calcRequiredData.lat = this.lat;
        calcRequiredData.lng = this.lng;
        calcRequiredData.key = 'gYdK$q12@as';
        this.coreService.calcSecChance(calcRequiredData)
            .subscribe(res => {
                this.markers = res;
                // format chance number
                for (let marker of this.markers) {
                    marker.chance = Number(marker.chance.toFixed(1))
                }
                this.postalcodeButtonOptions.active = false;
            }, err => {
                console.log(err);
                this.postalcodeButtonOptions.active = false;
            });
    }

    private _filter(value: string): string[] {
        /*
        let schListInit = [];
        schListInit.push('NANYANG PRIMARY SCHOOL'
        , 'TAO NAN SCHOOL'
        , 'AI TONG SCHOOL'
        , 'MAHA BODHI SCHOOL'
        , 'ROSYTH SCHOOL'
        , 'PEI CHUN PUBLIC SCHOOL'
        , 'HENRY PARK PRIMARY SCHOOL'
        , 'CHONGFU SCHOOL'
        , 'MEE TOH SCHOOL'
        , 'ST HILDAS PRIMARY SCHOOL'
        );
        */
        if (value) {
            const filterValue = value.toLowerCase();
            return this.list.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
        } else {
            //return schListInit;
        }
    }

    scrollDown() {
        this.pageScrollService.scroll({
            document: this.document,
            scrollTarget: '#map',
        });
    }

    initScore() {
        return this._formBuilder.group({
            math: ['', [Validators.required, Validators.max(100), Validators.min(0)]],
            science: ['', [Validators.required, Validators.max(100), Validators.min(0)]],
            eng: ['', [Validators.required, Validators.max(100), Validators.min(0)]],
            mt: ['', [Validators.required, Validators.max(100), Validators.min(0)]],
            //term: ['', Validators.required],
            //grade: ['', Validators.required]
            gradeterm: ['', Validators.required]
        });
    }

    addNewScoreSet() {
        const control = <FormArray>this.scoreFormGroup.controls['scores'];
        control.push(this.initScore());
        //console.log(this.scoreFormGroup);
    }

    removeScoreSet(i: number) {
        const control = <FormArray>this.scoreFormGroup.controls['scores'];
        control.removeAt(i);
    }

    checkResult() {
        this.googleAnalyticsService.eventEmitter('homepage', 'checktscore', 'btn', 1);
        this.spinnerLoading = true;
        this.isCollapsed = true;

        let reqData = new ScoreSets();
        reqData.school = this.schoolFormGroup['controls'].schoolName.value;
        localStorage.setItem('school', reqData.school);
        for (let i = 0; i < this.scoreFormGroup['controls'].scores['controls'].length; i++) {
            const score = this.scoreFormGroup['controls'].scores['controls'][i];
            let scoreSet = new ScoreSet();
            scoreSet.eng = score['controls'].eng.value;
            this.calculatedScore.eng = scoreSet.eng;
            scoreSet.grade = score['controls'].gradeterm.value.grade;
            this.calculatedScore.grade = scoreSet.grade;
            scoreSet.math = score['controls'].math.value;
            this.calculatedScore.math = scoreSet.math;
            scoreSet.mt = score['controls'].mt.value;
            this.calculatedScore.mt = scoreSet.mt;
            scoreSet.science = score['controls'].science.value;
            this.calculatedScore.science = scoreSet.science;
            scoreSet.term = score['controls'].gradeterm.value.term;
            this.calculatedScore.term = scoreSet.term;
            reqData.scores.push(scoreSet);
        }
        reqData.clientID = this.coreService.getClientSeed()
        reqData.sessionID = this.coreService.generateUnitId(24);

        const loggedIn = localStorage.getItem('userData');
        if (loggedIn != null) {
            this.coreService.getTscore(reqData)
                .subscribe(res => {
                    this.spinnerLoading = false;
                    this.isCollapsed = false;
                    this.score = Number(res.score.toFixed(0));
                    this.calculatedScore.score = this.score;
                    this.percent = res.percent.toFixed(1);
                    this.calculatedScore.percent = this.percent;
                    this.stdev = res.stdev;
                    this.calculatedScore.stdev = this.stdev;
                    this.isShowMap = true;
                    localStorage.setItem('calculatedscore', JSON.stringify(this.calculatedScore));
                }, err => {
                    console.log(err);
            });
        } else {
            this.coreService.getRestrictedTscore(reqData)
                .subscribe(res => {
                    if (res.status === 'Exceed Count') {
                        const alertString = `Max Usage Count exceeded. Unregistered user only
                        can use this service three times.`
                        let snackBarRef = this._snackBar.open(alertString, '', {
                            duration: 5000,
                        });
                        this.spinnerLoading = false;
                    } else {
                        this.spinnerLoading = false;
                        this.isCollapsed = false;
                        this.score = Number(res.score.toFixed(0));
                        this.calculatedScore.score = this.score;
                        this.percent = res.percent.toFixed(1);
                        this.calculatedScore.percent = this.percent;
                        this.stdev = res.stdev;
                        this.calculatedScore.stdev = this.stdev;
                        this.isShowMap = true;
                        localStorage.setItem('calculatedscore', JSON.stringify(this.calculatedScore));
                    }
                }, err => {
                    console.log(err);
            });
        }

    }

    openedWindow: number = 0; // alternative: array of numbers

    openWindow(id) {
        this.openedWindow = id; // alternative: push to array of numbers
    }

    isInfoWindowOpen(id) {
        return this.openedWindow == id; // alternative: check if id is in array
    }

}
