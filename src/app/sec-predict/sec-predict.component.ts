import { Component, OnInit, Inject, ViewChild, ElementRef, NgZone } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { UserService } from 'app/services/user.service';
import { CoreService } from 'app/services/core.service';
import { map, startWith } from 'rxjs/operators';
import { MatProgressButtonOptions } from 'mat-progress-buttons'
import { MapsAPILoader } from '@agm/core';
import { User } from 'app/services/user';
import { AuthService } from 'app/services/auth.service';
import { ScoreSets } from 'app/data/score-sets';
import { ScoreSet } from 'app/data/score-set';
import { CalcSeclistReq } from 'app/data/calc-seclist-req';
import { CalcSeclistRes } from 'app/data/calc-seclist-res';
import { CalculatedScore } from 'app/data/calc-score';
import { MatTableDataSource } from '@angular/material';
import { GoogleanalyticsService } from 'app/services/googleanalytics.service';

@Component({
  selector: 'app-sec-predict',
  templateUrl: './sec-predict.component.html',
  styleUrls: ['./sec-predict.component.scss']
})
export class SecPredictComponent implements OnInit {

  user: User;
  private primaryList = [];
  private secondaryList = [];
  primaryFilteredOptions: Observable<string[]>;
  secondaryFilteredOptions: Observable<string[]>;
  /*
  primarySchoolName: string;
  primarySchoolCtrl = new FormControl();
  secondarySchoolCtrl = new FormControl();
  gradetermCtrl = new FormControl();
  mathCtrl = new FormControl();
  scienceCtrl = new FormControl();
  engCtrl = new FormControl();
  mtCtrl = new FormControl();
  */
  secondarySchoolCtrl = new FormControl();

  score: number;
  stdev: number
  public gradeterms = [
    { display: 'P5-SA1', grade: 5, term: 'SA1' },
    { display: 'P5-SA2', grade: 5, term: 'SA2' },
    { display: 'P6-SA1', grade: 6, term: 'SA1' },
    { display: 'P6-Prelim', grade: 6, term: 'SA2' }];

  public lat: number;
  public lng: number;
  public zoom: number;
  public markers: Array<CalcSeclistRes> = [];

  queryButtonOptions: MatProgressButtonOptions = {
    active: false,
    text: 'Predict',
    buttonColor: 'primary',
    barColor: 'accent',
    raised: true,
    stroked: false,
    mode: 'indeterminate',
    value: 0,
    disabled: false
  }

  tableSource: any;
  displayedColumns: string[] = ['grade', 'math', 'science', 'eng', 'mt'];

  @ViewChild('search')
  public searchElementRef: ElementRef;

  constructor(
    private userService: UserService,
    private coreService: CoreService,
    private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,
    private authService: AuthService,
    public googleAnalyticsService: GoogleanalyticsService
  ) {
    this.user = this.authService.userData;
  }

  ngOnInit() {
    this.secondaryFilteredOptions = this.secondarySchoolCtrl.valueChanges.pipe(
      startWith(''),
      map(value => this._filter(value, 'secondary'))
    );
    this.coreService.getSchoolList()
      .subscribe(res => {
        this.primaryList = res.schools;
      }, err => {
        console.log(err);
      });
    this.coreService.getSecondaySchoolList()
      .subscribe(res => {
        this.secondaryList = res.schools;
      }, err => {
        console.log(err);
      });

    if (this.user) {
      //completeness check
      if (!this.user.school) {
        window.alert('Please Register Your Primary School into Profile Before You Start Prediction.');
      } else if (!this.user.postalCode) {
        window.alert('Please Register Your Home Postal Code into Profile Before You Start Prediction.');
      } else if (this.user.scores.length == 0) {
        window.alert('Please Register Your Scores into Profile Before You Start Prediction.');
      }
      /*
      this.primarySchoolCtrl.setValue(this.user.school);
      if (this.user.scores.length > 0) {
        let scores = this.user.scores.sort(function (a, b) {
          if (a.grade < b.grade) {
            return 1;
          }
          if (a.grade > b.grade) {
            return -1;
          }
          if (a.term < b.term) {
            return 1;
          }
          if (a.term > b.term) {
            return -1;
          }
          return 0;
        });
        const grade = scores[0].grade;
        const term = scores[0].term;
        for (let data of this.gradeterms) {
          if (data.grade == grade && data.term == term) {
            this.gradetermCtrl.setValue(data);
          }
        }
        this.mathCtrl.setValue(scores[0].math);
        this.scienceCtrl.setValue(scores[0].science);
        this.engCtrl.setValue(scores[0].eng);
        this.mtCtrl.setValue(scores[0].mt);
      }
      */
      this.tableSource = new MatTableDataSource<CalculatedScore>(this.user.scores);

      this.getScore();
    }

    this.lat = 1.3521;
    this.lng = 103.8198;
    this.zoom = 11;
    //load Places Autocomplete
    this.mapsAPILoader.load().then(() => {
      //this.setCurrentLocation();
      //this.geoCoder = new google.maps.Geocoder;
      let autocomplete = new google.maps.places.Autocomplete(this.searchElementRef.nativeElement, {
        types: ["school"]
      });
      autocomplete.setComponentRestrictions(
        { 'country': ['sg'] });
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
          //set latitude, longitude and zoom
          this.lat = place.geometry.location.lat();
          this.lng = place.geometry.location.lng();
          this.zoom = 14;
        });
      });
    });
  }
  private _filter(value: string, type: string): string[] {
    if (value) {
      const filterValue = value.toLowerCase();
      if (type === 'primary') {
        return this.primaryList.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
      } else {
        return this.secondaryList.filter(option => option.toLowerCase().indexOf(filterValue) === 0);
      }
    }
  }
  query() {
    this.googleAnalyticsService.eventEmitter('predictpage', 'secchancequery', 'btn', 1);
    this.queryButtonOptions.active = true;
    // get prediction
    let reqSecList = new CalcSeclistReq();
    reqSecList.clientID = this.coreService.getClientSeed();
    reqSecList.uid = this.user.uid;
    reqSecList.score = this.score;
    reqSecList.stdev = this.stdev;
    reqSecList.sec_sch_list.push(this.secondarySchoolCtrl.value);
    this.coreService.getSecondaryPrediction(reqSecList)
      .subscribe(resSec => {
        //this.markers = resSec;
        for (let data of resSec) {
          this.markers.push(data);
        }
        // format chance number
        for (let marker of this.markers) {
          marker.chance = Number(marker.chance.toFixed(1))
        }
        this.queryButtonOptions.active = false;
      }, err => {
        console.log(err);
      });
  }

  reset() {
    this.markers = [];
  }

  openedWindow: number = 0; // alternative: array of numbers

  openWindow(id) {
    this.openedWindow = id; // alternative: push to array of numbers
  }

  isInfoWindowOpen(id) {
    return this.openedWindow == id; // alternative: check if id is in array
  }

  getScore() {
    // get t-score
    let reqData = new ScoreSets();
    reqData.school = this.user.school;
    reqData.clientID = this.coreService.getClientSeed();
    for (let data of this.user.scores) {
      let scoreSet = new ScoreSet();
      scoreSet.eng = data.eng;
      scoreSet.grade = data.grade;
      scoreSet.term = data.term;
      scoreSet.math = data.math;
      scoreSet.mt = data.mt;
      scoreSet.science = data.science;
      reqData.scores.push(scoreSet);
    }
    this.coreService.getTscore(reqData)
      .subscribe(res => {
        this.score = Number(res.score.toFixed(1));
        this.stdev = res.stdev;
      }, err => {
        console.log(err);
      });
  }

}
