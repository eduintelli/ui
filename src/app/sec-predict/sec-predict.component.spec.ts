import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SecPredictComponent } from './sec-predict.component';

describe('SecPredictComponent', () => {
  let component: SecPredictComponent;
  let fixture: ComponentFixture<SecPredictComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SecPredictComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SecPredictComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
