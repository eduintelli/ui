export class CalcSeclistRes {
    chance: number;
    id: number;
    lat: number;
    lng: number;
    sec_sch_name: string;
}
