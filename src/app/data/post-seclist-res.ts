export class PostSeclistRes {
    chance: number;
    cum: number;
    cut_off: number;
    dist: number;
    gender: string;
    id: number;
    lat: number;
    lng: number;
    sec_sch_name: string;
    label: string;
}
