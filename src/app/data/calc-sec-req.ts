export class CalcSecReq {
    score: number;
    stdev: number;
    lat: number;
    lng: number;
    key: string
}
