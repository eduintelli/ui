export class ScoreSet {
    math: number;
    science: number;
    eng: number;
    mt: number;
    term: string;
    grade: number;
}
