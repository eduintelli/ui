export class PostSeclistReq {
    score: number;
    gender: string;
    lat: number;
    lng: number;
    clientID: string;
    sessionID: string;
    key: string;
    uid: string;

    constructor() {
        this.key = 'gYdK$q12@as';
        this.sessionID = Math.random().toString(36).substr(2, 9);
    }
}
