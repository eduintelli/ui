import { CalcSecResult } from './calc-sec-result';

export class CalcSecSummary {
    results: CalcSecResult[]
}
