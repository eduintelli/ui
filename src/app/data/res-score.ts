export class ResScore {
    status: string;
    score: number;
    percent: number;
    stdev: number
}
