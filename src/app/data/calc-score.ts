export class CalculatedScore {
    math: number;
    science: number;
    eng: number;
    mt: number;
    term: string;
    grade: number;
    score: number;
    percent: string;
    stdev: number;
}
