export class CalcSecResult {
    chance: number;
    lat: number;
    lng: number;
    sec_sch_name: string;
    id: number
}
